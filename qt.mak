.PHONY: qt_all qt_configure qt_uninstall qt_distclean qt_update qt_confclean qt_reconfigure qt_patch

DIRECTORIES_TO_CREATE += $(QT_HOSTPREFIX)

qt_all: qt_configure qt_build qt_install

$(QT_DIR)/qt.pro qt_update:
	$(Q)$(SCRIPTS_DIR)/qt_prepare.sh "$(QT_DIR)"
	$(Q)$(MAKE) qt_patch

SPLITTER := _
.PHONY: $(foreach target, build install clean, qt_$(target) $(call foreach_qt_submodule,,$(SPLITTER)$(target)) $(call foreach_non_qt_submodule,,$(SPLITTER)$(target))) $(call foreach_non_qt_submodule,,$(SPLITTER)config)

QT_SUBMODULES_REV := $(call reverse,$(QT_SUBMODULES))
foreach_qt_submodule = $(foreach module, qtbase $(QT_SUBMODULES), $(1)$(module)$(2))
foreach_qt_submodule_rev = $(foreach module, $(QT_SUBMODULES_REV) qtbase, $(1)$(module)$(2))
NON_QT_SUBMODULES_REV := $(call reverse,$(NON_QT_SUBMODULES))
foreach_non_qt_submodule = $(foreach module, $(NON_QT_SUBMODULES), $(1)$(module)$(2))
foreach_non_qt_submodule_rev = $(foreach module, $(NON_QT_SUBMODULES_REV), $(1)$(module)$(2))

$(call foreach_non_qt_submodule,,$(SPLITTER)config): %$(SPLITTER)config: $(QT_HOSTPREFIX)/%/Makefile $(QT_VERSION_FILE)
$(foreach target, build install clean, $(call foreach_qt_submodule,,$(SPLITTER)$(target)) $(call foreach_non_qt_submodule,,$(SPLITTER)$(target))): $(QT_VERSION_FILE)

MANDATORY_PACKAGES_qtwebkit = ruby gperf
MANDATORY_PACKAGES_qtbase = g++

define add_build_prerequisites
MANDATORY_PACKAGES += $(MANDATORY_PACKAGES_$(1))
endef

$(foreach module, qtbase $(QT_SUBMODULES), $(eval $(call add_build_prerequisites,$(module))))

qt_configure: $(QT_HOSTPREFIX)/Makefile $(call foreach_non_qt_submodule,,$(SPLITTER)config)

qt_confclean:
	@echo Cleaning Qt configuration ...
	$(Q)rm -rf "$(QT_HOSTPREFIX)" "$(QT_VERSION_FILE)" 2>/dev/null
	$(Q)cd "$(QT_DIR)" && git submodule foreach "git clean -dxf && git checkout -f"
	$(Q)$(call foreach_non_qt_submodule, cd "$(QT_DIR)"/, && git clean -dfx && git checkout -f && ) true

qt_patch:
	@echo Patching Qt ...
	$(Q)retcode=0 && ls -1d "$(QT_DIR)"/*/ | while read QTMOD_DIR; \
       	do \
	    cd "$$QTMOD_DIR" && \
	    for P in $$(ls -1 "$(EMBUILD_BASE)/patches/qt/$${PWD##*/}"_*.patch 2>/dev/null); \
	    do \
	        echo Applying patch $$P...;  \
		patch -p1 < "$$P" || retcode=$$?; \
	    done && \
	    [ 0 = $$retcode ]; \
	done
	$(Q)cp -rf "$(EMBUILD_BASE)/patches/qt/linux-arm-odroid-g++" "$(QT_DIR)/qtbase/mkspecs/devices"

qt_reconfigure: qt_confclean qt_patch qt_configure

$(QT_HOSTPREFIX)/Makefile: $(QT_DIR)/qt.pro $(QT_VERSION_FILE) 
	@echo Configuring Qt $(QT_VER) ...
	$(Q)$(SCRIPTS_DIR)/qt_configure.sh

$(foreach module, $(NON_QT_SUBMODULES), $(QT_HOSTPREFIX)/$(module)/Makefile):
	$(Q)[ -d "$(@D)" ] || mkdir -p "$(@D)"
	$(Q)cd $(@D) && $(QT_HOSTPREFIX)/qtbase/bin/qmake $(subst $(QT_HOSTPREFIX),$(QT_DIR),$(@D))/$(notdir $(@D)).pro -o Makefile

qt_build: $(call foreach_qt_submodule,,$(SPLITTER)build) $(call foreach_non_qt_submodule,,$(SPLITTER)build)

$(call foreach_qt_submodule,,$(SPLITTER)build):
	@echo Building Qt $(QT_VER) submodule $(@:%$(SPLITTER)build=%)...
	$(Q)$(MAKE) -C $(QT_HOSTPREFIX) $(HARDWARE_CONCURRENCY) module-$(@:%$(SPLITTER)build=%)-make_first
	$(Q)$(MAKE) -C $(QT_HOSTPREFIX) $(HARDWARE_CONCURRENCY) module-$(@:%$(SPLITTER)build=%)-all

$(call foreach_non_qt_submodule,,$(SPLITTER)build):
	@echo Building Qt $(QT_VER) extra submodule $(@:%$(SPLITTER)build=%)...
	$(Q)$(MAKE) -C $(QT_HOSTPREFIX)/$(@:%$(SPLITTER)build=%) $(HARDWARE_CONCURRENCY) qmake
	$(Q)$(MAKE) -C $(QT_HOSTPREFIX)/$(@:%$(SPLITTER)build=%) $(HARDWARE_CONCURRENCY) all

qt_install: $(call foreach_qt_submodule,,$(SPLITTER)install) $(call foreach_non_qt_submodule,,$(SPLITTER)install)
	$(Q)$(SCRIPTS_DIR)/qt_postinstall.sh

$(call foreach_qt_submodule,,$(SPLITTER)install):
	@echo Installing Qt $(QT_VER) submodule $(@:%$(SPLITTER)install=%)...
	$(Q)$(MAKE) -C $(QT_HOSTPREFIX) $(HARDWARE_CONCURRENCY) module-$(@:%$(SPLITTER)install=%)-install_subtargets

$(call foreach_non_qt_submodule,,$(SPLITTER)install):
	@echo Installing Qt $(QT_VER) extra submodule $(@:%$(SPLITTER)install=%)...
	$(Q)$(MAKE) -C $(QT_HOSTPREFIX)/$(@:%$(SPLITTER)install=%) $(HARDWARE_CONCURRENCY) install

qt_clean: $(call foreach_non_qt_submodule_rev,,$(SPLITTER)clean) $(call foreach_qt_submodule_rev,,$(SPLITTER)clean)

$(call foreach_qt_submodule,,$(SPLITTER)clean):
	@echo Cleaning Qt $(QT_VER) submodule $(@:%$(SPLITTER)clean=%)...
	$(Q)$(MAKE) -C $(QT_HOSTPREFIX) $(HARDWARE_CONCURRENCY) module-$(@:%$(SPLITTER)clean=%)-clean

$(call foreach_non_qt_submodule,,$(SPLITTER)clean):
	@echo Cleaning Qt $(QT_VER) extra submodule $(@:%$(SPLITTER)clean=%)...
	$(Q)$(MAKE) -C $(QT_HOSTPREFIX)/$(@:%$(SPLITTER)clean=%) $(HARDWARE_CONCURRENCY) clean

qt_distclean: qt_confclean
	-$(Q)rm -rf "$(QT_DIR)"

qt_uninstall:
	$(Q)rm -rf "$(QT_INSTALL_DIR)"

help-common: $(QT_VERSION_FILE)

help: help-qt
help-qt:
	@echo " "
	@echo "   qt_all              - configures, builds and installs Qt $(QT_VER) into target root filesystem;"
	@echo "   qt_configure        - configures Qt;"
	@echo "   qt_configure        - re-configures Qt;"
	@echo "   qt_build            - incrementally builds Qt;"
	@echo "   qt_install          - installs Qt into target root filesystem;"
	@echo "   qt_uninstall        - uninstalls Qt from target root filesystem;"
	@echo "   qt_clean            - cleans Qt leaving build configuration files;"
	@echo "   qt_distclean        - completely cleans Qt;"
	@echo "   qt_update           - does clean update of Qt source files;"
	@echo "   qt_confclean        - cleans up Qt onfiguration;"

all: qt_all
clean: qt_clean
distclean: qt_distclean

