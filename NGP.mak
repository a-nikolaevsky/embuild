.PHONY: ngp_all ngp_clean ngp_distclean ngp_target ngp_printenv ngp_install_platform_sdk ngp_uninstall_platform_sdk ngp_setup_sysroot ngp_binary ngp_package ngp_sdk_all ngp_sdk_target ngp_sdk_clean

NGP_HWPLATFORM := $(shell echo $(BOARD_NAME) | tr [A-Z] [a-z] | sed 's@[- ]@@g')
HWPLATFORM_SDK_HOME ?= $(BUILD_ROOT)/$(NGP_HWPLATFORM)_platform_sdk

MANDATORY_PACKAGES_ngp = xsltproc preprocess realpath gawk fakeroot dpkg-dev
MANDATORY_PACKAGES_ngp_sdk = g++ automake autoconf libtool gettext intltool bison flex
MANDATORY_PACKAGES += $(MANDATORY_PACKAGES_ngp) $(MANDATORY_PACKAGES_ngp_sdk)

all: ngp_sdk_all ngp_all
clean: ngp_clean ngp_sdk_clean
distclean: ngp_distclean

ngp_all: ngp_install_platform_sdk ngp_setup_sysroot ngp_binary ngp_package
ngp_clean ngp_distclean: ngp_uninstall_platform_sdk

ngp_setup_sysroot: | $(NGP_SDK_ROOT)/$(NGP_HWPLATFORM)/sysroot

$(NGP_SDK_ROOT)/$(NGP_HWPLATFORM)/sysroot:
	$(Q)ln -s "$(TARGETFS_INSTALL_DIR)" $(NGP_SDK_ROOT)/$(NGP_HWPLATFORM)/sysroot

ngp_install_platform_sdk: 
	$(Q)$(MAKE) kernel_headers_install INSTALL_HDR_PATH="$(HWPLATFORM_SDK_HOME)"

ngp_uninstall_platform_sdk:
	$(Q)rm -rf "$(HWPLATFORM_SDK_HOME)"

ngp_target:
	$(Q)env PATH="$(CROSS_COMPILE_DIR)/bin:$$PATH" CROSS_PKG_CONFIG_ADD_SYSROOT_TO_VARIABLES=1 $(call with_QT) \
	$(MAKE) -C "$(NGP_HOME)" $(if $(NGP_SDK_ROOT),SDK_ROOT="$(NGP_SDK_ROOT)") SYSROOT_DIR="$(TARGETFS_INSTALL_DIR)" \
	    release=$(NGP_RELEASE) HWPLATFORM_SDK_HOME="$(HWPLATFORM_SDK_HOME)" $(HARDWARE_CONCURRENCY) $(NGP_HWPLATFORM)-$(TARGET)

ngp_clean ngp_distclean ngp_binary ngp_package:
	$(Q)$(MAKE) TARGET=$(@:ngp_%=%) ngp_target

ngp_printenv:
	$(Q)$(MAKE) TARGET=print-env ngp_target

ngp_sdk_target:
	$(Q)env PATH="$(CROSS_COMPILE_DIR)/bin:$$PATH" CROSS_PKG_CONFIG_ADD_SYSROOT_TO_VARIABLES=1 \
	$(MAKE) -C "$(NGP_SDK_BUILD_ROOT)" SYSROOT="$(TARGETFS_INSTALL_DIR)" HWPLATFORM=$(NGP_HWPLATFORM) $(TARGET)

ngp_sdk_all:
	$(Q)$(MAKE) TARGET=sdk ngp_sdk_target

ngp_sdk_clean:
	$(Q)$(MAKE) TARGET=clean ngp_sdk_target

help: help-ngp
help: help-ngp
help-ngp:
	@echo " "
	@echo "   ngp_all             - builds NGP;"
	@echo "   ngp_clean           - cleans NGP;"
	@echo "   ngp_distclean       - cleans NGP;"
	@echo "   ngp_printenv        - prints NGP build environment;"
	@echo "   ngp_install_platform_sdk"
	@echo "                       - installs $(NGP_HWPLATFORM) platform SDK necessary for NGP building into HWPLATFORM_SDK_HOME=$(HWPLATFORM_SDK_HOME);"
	@echo "   ngp_uninstall_platform_sdk"
	@echo "                       - uninstalls $(NGP_HWPLATFORM) platform SDK;"
	@echo "   ngp_target          - forwards target specified through TARGET to NGP build makefile;"
	@echo "   ngp_sdk_target      - forwards target specified through TARGET to NGPSDK build makefile;"
	@echo "   ngp_sdk_all         - builds NGP SDK;"
	@echo "   ngp_sdk_clean       - cleans NGP SDK;"

