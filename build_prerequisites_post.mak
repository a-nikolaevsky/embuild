### COMPILER INSTALLATION ###

.PHONY: setup-emdebian-compiler setup-linaro-compiler setup-default-compiler

setup-default-compiler: setup-linaro-compiler

help: help-compiler-alternatives
help-compiler-alternatives:
	@echo " "
	@echo "   setup-default-compiler"
	@echo "                       - sets up default cross-compiler to be used;"
	@echo "   setup-emdebian-compiler"
	@echo "                       - sets up Emdebian $(TARGET_ARCH) cross-compiler to be used;"
	@echo "   setup-linaro-compiler"
	@echo "                       - sets up Linaro $(TARGET_ARCH) cross-compiler to be used;"

####### EMDEBIAN #######
EMDEBIAN_GCC_VERSION = 4.7
EMDEBIAN_GCC_PREFIX = /usr/bin/$(CROSS_COMPILE_TRIPLET)-
setup-emdebian-compiler: $(BUILD_PREREQUISITES_DIR) $(BUILD_PREREQUISITES_DIR)/emdebian.installed $(BUILD_PREREQUISITES_DIR)/emdebian-compiler.installed
$(BUILD_PREREQUISITES_DIR)/emdebian-compiler.installed:
	@echo Setting up Emdebian $(TARGET_ARCH) cross compiler ...
	$(Q)$(SCRIPTS_DIR)/install_files_from_package -p g++-$(EMDEBIAN_GCC_VERSION)-$(CROSS_COMPILE_TRIPLET) -a $(HOST_MULTIARCH) -s
	$(Q)i=0; find $(dir $(EMDEBIAN_GCC_PREFIX)) -regextype posix-awk -regex "$(EMDEBIAN_GCC_PREFIX)gcc-[0-9]{1,}.[0-9]{1,}" | sed 's@$(EMDEBIAN_GCC_PREFIX)gcc-@@' | { \
	    while read V; do \
		sudo update-alternatives --install $(EMDEBIAN_GCC_PREFIX)gcc $(CROSS_COMPILE_TRIPLET)-gcc $(EMDEBIAN_GCC_PREFIX)gcc-$${V} $${i} $$(for F in $(EMDEBIAN_GCC_PREFIX)*-$${V}; do [ $${F} = $(EMDEBIAN_GCC_PREFIX)gcc-$${V} ] || echo "--slave $${F%-$${V}} $$(basename $${F%-$${V}}) $${F}"; done) || exit $$?; \
		((i=i+1)); \
	    done \
	} && sudo update-alternatives --set $(CROSS_COMPILE_TRIPLET)-gcc $(EMDEBIAN_GCC_PREFIX)gcc-$(EMDEBIAN_GCC_VERSION) 
	$(Q)echo 'CROSS_COMPILE_DIR := /usr' >> "$(EMBUILD_BASE)/.env.mak"
	$(Q)touch "$@"

###### LINARO ######
LAST_KNOWN_LINARO_RELEASE_BUNDLING_REQUIRED_GCC_VERSION = 2013.04
setup-linaro-compiler: $(BUILD_PREREQUISITES_DIR) $(BUILD_PREREQUISITES_DIR)/linaro-compiler.installed
$(BUILD_PREREQUISITES_DIR)/linaro-compiler.installed: $(BUILD_PREREQUISITES_DIR)/bzip2.installed
	@echo Setting up Linaro $(TARGET_ARCH) cross compiler ...
	$(Q)-mkdir -p $(DOWNLOADS_DIR)/compiler/Linaro
	$(Q)wget --quiet --no-check-certificate -O - https://launchpad.net/linaro-toolchain-binaries$(if $(LAST_KNOWN_LINARO_RELEASE_BUNDLING_REQUIRED_GCC_VERSION),/+milestone/$(LAST_KNOWN_LINARO_RELEASE_BUNDLING_REQUIRED_GCC_VERSION)) | \
	perl -n -e 'print $$1, "\n" if /\s+href="([^"]+)"/' | \
	grep arm-linux-gnueabihf$(if $(LAST_KNOWN_LINARO_RELEASE_BUNDLING_REQUIRED_GCC_VERSION),-$(EMDEBIAN_GCC_VERSION)) | grep _linux.tar.bz2 | { \
	    read PKG_URI && \
	    PKG_NAME=$${PKG_URI##*/} && \
	    PKG="$(DOWNLOADS_DIR)/compiler/Linaro/$$PKG_NAME" && \
	    if [ ! -e "$$PKG" ]; then echo "Downloading $$PKG_URI ..." && wget --no-check-certificate -O "$$PKG" $$PKG_URI; fi && \
	    echo "Extracting $$PKG ..." && tar -xf "$$PKG" -C $$HOME && \
	    echo "CROSS_COMPILE_DIR := $$HOME/$${PKG_NAME%.tar.bz2}" >> "$(EMBUILD_BASE)/.env.mak"; \
	}
	$(Q)touch "$@"

ifneq ($(HOST_MULTIARCH),i386)

DEPENDENCY_PACKAGES += ia32-libs

$(BUILD_PREREQUISITES_DIR)/linaro-compiler.installed: $(BUILD_PREREQUISITES_DIR)/ia32-libs.installed
$(BUILD_PREREQUISITES_DIR)/ia32-libs.installed: $(BUILD_PREREQUISITES_DIR)/i386.installed 

$(BUILD_PREREQUISITES_DIR)/i386.installed:
	@echo Ensuring architecture i386 is supported
	$(Q)dpkg --print-foreign-architectures | grep -qx i386 || ( sudo dpkg --add-architecture i386 && sudo apt-get update )
	$(Q)touch "$@"

endif

### END COMPILER INSTALLATION ###

### PACKAGE REQUIREMENTS ###

define ensure_package_is_installed
	$(Q)$(SCRIPTS_DIR)/install_files_from_package -p $(1) -a $(HOST_MULTIARCH) -s
endef

define ensure_package_is_installed_target
$(BUILD_PREREQUISITES_DIR)/$(1).installed:
	@echo Ensuring package $(1) is installed ...
	$$(call ensure_package_is_installed,$(1))
	$(Q)touch "$$@"

.PHONY: ensure-package-$(1)-is-installed
ensure-package-$(1)-is-installed: $(BUILD_PREREQUISITES_DIR)/$(1).installed
endef

$(foreach pkg, $(sort $(MANDATORY_PACKAGES) $(DEPENDENCY_PACKAGES)), \
    $(eval $(call ensure_package_is_installed_target,$(pkg)))\
)

check-build-prerequisites: gfx_sdk-check-build-prerequisites $(foreach pkg, $(MANDATORY_PACKAGES), $(BUILD_PREREQUISITES_DIR)/$(pkg).installed)

$(BUILD_PREREQUISITES_DIR)/multistrap.installed: $(BUILD_PREREQUISITES_DIR)/emdebian.installed
$(BUILD_PREREQUISITES_DIR)/emdebian.installed:
	@echo Ensuring Emdebian package repository is installed ...
	$(Q)egrep "^ *deb " /etc/apt/sources.list /etc/apt/sources.list.d/*.list 2>/dev/null | fgrep -q http://www.emdebian.org/debian || ( \
	sudo apt-get install emdebian-archive-keyring && \
	for suite in squeeze testing unstable; \
	do \
	    echo "deb [arch=$(HOST_MULTIARCH)] http://www.emdebian.org/debian $$suite main" | sudo tee -a /etc/apt/sources.list.d/emdebian.list; \
	done && sudo apt-get update )
	$(Q)touch $@

# fakeroot and fakechroot libraries for target architecture are required
# see http://lists.debian.org/debian-embedded/2011/06/msg00111.html for details
check-build-prerequisites: $(BUILD_PREREQUISITES_DIR)/fakeroot-libs-$(TARGET_ARCH).installed
$(BUILD_PREREQUISITES_DIR)/fakeroot-libs-$(TARGET_ARCH).installed:
	@echo Ensuring fakeroot libraries for architecture $(TARGET_ARCH) are available ...
	$(Q)$(SCRIPTS_DIR)/install_files_from_package -p fakeroot -a $(TARGET_ARCH) -s -l /usr/lib/$(CROSS_COMPILE_TRIPLET) ./usr/lib/$(CROSS_COMPILE_TRIPLET)/libfakeroot/libfakeroot-sysv.so
	$(Q)touch "$@"

check-build-prerequisites: $(BUILD_PREREQUISITES_DIR)/fakechroot-libs-$(TARGET_ARCH).installed
$(BUILD_PREREQUISITES_DIR)/fakechroot-libs-$(TARGET_ARCH).installed:
	@echo Ensuring fakechroot libraries for architecture $(TARGET_ARCH) are available ...
	$(Q)$(SCRIPTS_DIR)/install_files_from_package -p libfakechroot -a $(TARGET_ARCH) -s -l /usr/lib/$(CROSS_COMPILE_TRIPLET) ./usr/lib/$(CROSS_COMPILE_TRIPLET)/fakechroot/libfakechroot.so
	$(Q)touch "$@"

### END PACKAGE REQUIREMENTS ###
	
### COMPILER SANITY CHECK ###

PATH_PSEUDO_SEPARATOR := ,
COMPILER_USABLE := $(BUILD_PREREQUISITES_DIR)/compiler.$(subst /,$(PATH_PSEUDO_SEPARATOR),$(realpath $(CROSS_COMPILE)gcc)).usable
check-build-prerequisites: $(COMPILER_USABLE)
$(COMPILER_USABLE):
	@echo Ensuring compiler $(CROSS_COMPILE)gcc is usable
	$(Q)echo 'int main(){return 0;}' | $(CROSS_COMPILE)gcc -pipe -o "$(COMPILER_USABLE)" -x c -

### END COMPILER SANITY CHECK ###

