.PHONY: rootfs rootfs_clean rootfs_package rootfs_package_clean rootfs_package_all rootfs_package_update rootfs_bootable_snapshot rootfs_bootable_snapshot_sync rootfs_shell

TARGETFS_AVAILABILITY_INDICATOR = $(TARGETFS_INSTALL_DIR)/etc/inittab
rootfs: $(TARGETFS_INSTALL_DIR) $(TARGETFS_AVAILABILITY_INDICATOR)

DIRECTORIES_TO_CREATE += $(TARGETFS_INSTALL_DIR)

MANDATORY_PACKAGES += multistrap bzip2 zip unzip qemu-user-static fakeroot fakechroot realpath lsb-release gawk

rootfs_clean:
	@echo Cleaning target root filesystem...
	$(Q)[ ! -e "$(TARGETFS_INSTALL_DIR)" ] || $(SCRIPTS_DIR)/update_rootfs.sh -s "$(TARGETFS_INSTALL_DIR)" invoke rootfs_remove

rootfs_package_all: rootfs_package_clean rootfs_package

rootfs_package: $(BUILD_ROOT) $(ROOTFS_PACKAGE)

$(ROOTFS_PACKAGE):
	$(Q)$(SCRIPTS_DIR)/create_rootfs --arch $(TARGET_ARCH) --proxy "$(PACKAGE_REPOSITORY_PROXY)" --package "$@" $(TARGET_PLATFORM)

rootfs_package_clean:
	-$(Q)$(SCRIPTS_DIR)/update_rootfs.sh -s "$(ROOTFS_PACKAGE)" rootfs_remove 

rootfs_package_update:
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh -s "$(TARGETFS_INSTALL_DIR)" -a $(TARGET_ARCH) -p "$(ROOTFS_PACKAGE)"

rootfs_bootable_snapshot:
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh -s "$(TARGETFS_INSTALL_DIR)" -a $(TARGET_ARCH) invoke rootfs_sync_to_bootable_snapshot "$(ROOTFS_BOOTABLE_SNAPSHOT_DIR)"

rootfs_bootable_snapshot_sync: $(ROOTFS_BOOTABLE_SNAPSHOT_DIR) $(TARGETFS_INSTALL_DIR)
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh -s "$(TARGETFS_INSTALL_DIR)" -a $(TARGET_ARCH) invoke rootfs_sync_from_bootable_snapshot "$(ROOTFS_BOOTABLE_SNAPSHOT_DIR)"

rootfs_shell:
	@echo Invoking root filesystem shell ...
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh -s "$(TARGETFS_INSTALL_DIR)" -a $(TARGET_ARCH) invoke rootfs_run $(TARGET)

$(TARGETFS_AVAILABILITY_INDICATOR):
	@echo Creating target root filesystem for platform $(TARGET_PLATFORM) ...
	$(Q)$(MAKE) rootfs_package
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh -s "$(TARGETFS_INSTALL_DIR)" invoke rootfs_extract "$(ROOTFS_PACKAGE)" 

help: help-rootfs
help-rootfs:
	@echo " "
	@echo "   rootfs              - creates target root filesystem;"
	@echo "   rootfs_clean        - removes target root filesystem;"
	@echo " "
	@echo "   rootfs_package_all  - (re-)creates basic root filesystem package;"
	@echo "   rootfs_package      - creates basic root filesystem package if necessary;"
	@echo "   rootfs_package_clean"
	@echo "                       - removes basic root filesystem package;"
	@echo "   rootfs_package_update"
	@echo "                       - updates basic root filesystem package from current target filesystem;"
	@echo "   rootfs_bootable_snapshot"
	@echo "                       - creates root filesystem snapshot bootable over NFS (ROOTFS_BOOTABLE_SNAPSHOT_DIR);"
	@echo "   rootfs_bootable_snapshot_sync"
	@echo "                       - synchronizes root filesystem with changes made in snapshot bootable over NFS;"
	@echo "   rootfs_shell        - runs shell inside root filesystem (arguments may be passed through TARGET);"

all: rootfs
distclean: rootfs_clean

