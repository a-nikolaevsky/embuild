where-am-i = $(abspath $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)) )
reverse = $(if $(1),$(call reverse,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1))

EMBUILD_BASE := $(patsubst %/,%, $(dir $(call where-am-i) ) )
TARGETFS_INSTALL_DIR := $(EMBUILD_BASE)/target/rootfs

BOARD_NAME := Odroid-U3
BOARD_TYPE := odroidu

-include $(EMBUILD_BASE)/.env.mak

BUILD_ROOT := $(EMBUILD_BASE)/build
SCRIPTS_DIR := $(EMBUILD_BASE)/scripts
ROOTFS_BOOTABLE_SNAPSHOT_DIR ?= $(TARGETFS_INSTALL_DIR).boot
ROOTFS_PACKAGE ?= $(BUILD_ROOT)/rfs-$(TARGET_PLATFORM)-$(TARGET_ARCH).tar.gz

DOWNLOADS_DIR ?= $(EMBUILD_BASE)/downloads
SUPPORT_XORG ?= 1

GFX_SDK_VER ?= 2.4.4
GFX_SDK_DIR ?= $(EMBUILD_BASE)/gfx_sdk/Mali_OpenGL_ES_SDK_v$(GFX_SDK_VER)
GFX_SDK_VENDOR := ARM
GFX_SDK_DEVICE := Mali 400MP
GFX_SDK_BIN_INSTALL_CMD ?= $(SCRIPTS_DIR)/gfx_binaries_install.sh $(EMBUILD_BASE)/gfx_sdk $(GFX_SDK_VER) $(DOWNLOADS_DIR)

QT_DIR ?= $(EMBUILD_BASE)/qt
QT_HOSTPREFIX ?= $(BUILD_ROOT)/qt
ifeq ($(QT_HOSTPREFIX), $(QT_DIR))
    $(error QT_HOSTPREFIX and QT_DIR must not be equal)
endif
QT_VER ?= 5.4.0
QT_TARGETFS_DIR ?= /opt/qt-$(QT_VER)
ifneq ($(filter $QT_TARGETFS_DIR,/usr /usr/local),)
   $(error Varibale QT_TARGETFS_DIR(=$(QT_TARGETFS_DIR)) should not point to system directory)
endif
QT_SUBMODULES ?= qtsvg qtxmlpatterns qtdeclarative qtquickcontrols qtmultimedia #qtwebkit qt3d 
NON_QT_SUBMODULES ?= qtbase/examples qtdeclarative/examples qtmultimedia/examples #qtwebengine qtwebkit-examples qtwebkit-examples/examples qt3d/examples
QMAKE = $(QT_HOSTPREFIX)/bin/qmake
QMAKESPEC = linux-arm-odroid-g++
with_QT = QMAKE="$(QMAKE)" QMAKESPEC=$(QMAKESPEC)

QT_INSTALL_DIR := $(TARGETFS_INSTALL_DIR)$(QT_TARGETFS_DIR)
QT_LIB := $(QT_INSTALL_DIR)/lib
QT_INC := $(QT_INSTALL_DIR)/include

CROSS_COMPILE_DIR ?= /usr
CROSS_COMPILE_TRIPLET = arm-linux-gnueabihf
CROSS_COMPILE_PREFIX = $(CROSS_COMPILE_TRIPLET)-
CROSS_COMPILE := $(CROSS_COMPILE_DIR)/bin/$(CROSS_COMPILE_PREFIX)
ARM_FLOAT_ABI := hard
TARGET_ARCH := armhf
TARGET_PLATFORM ?= debian

MULTIARCH_CROSSTOOL_BUG ?= 0
ifeq ($(MULTIARCH_CROSSTOOL_BUG),1)
   export MULTIARCH_CROSS_COMPILE_BUG_FIX := $(foreach dir, $(foreach syspath, /usr/include, $(TARGETFS_INSTALL_DIR)$(syspath)/$(CROSS_COMPILE_TRIPLET) $(TARGETFS_INSTALL_DIR)$(syspath)), -I$(dir) )
   export MULTIARCH_CROSS_LINK_BUG_FIX := $(foreach dir, $(foreach syspath, /lib /usr/lib, $(TARGETFS_INSTALL_DIR)$(syspath)/$(CROSS_COMPILE_TRIPLET) $(TARGETFS_INSTALL_DIR)$(syspath)), -L$(dir) -Wl,-rpath-link=$(dir) )
endif

PLATFORM_FLAGS := -marm -march=armv7-a -mtune=cortex-a9 -mfpu=neon --sysroot=$(TARGETFS_INSTALL_DIR) -mfloat-abi=$(ARM_FLOAT_ABI) -fstack-protector -ftree-vectorize -mvectorize-with-neon-quad #-ffast-math 
LINUX_DEVKIT_PATH := $(TARGETFS_INSTALL_DIR)
LINUXKERNEL_INSTALL_DIR ?= $(EMBUILD_BASE)/linux
LINUX_KERNEL_VERSION := $(shell perl -ne '$$mid=$$1 >> 8, print $$mid >> 8, ".", $$mid & 0xff, ".", $$1 & 0xff if /^\s*\#define\s+LINUX_VERSION_CODE\s+(\d+)/' "$(LINUXKERNEL_INSTALL_DIR)/include/linux/version.h" 2>/dev/null)
ifeq ($(LINUX_KERNEL_VERSION),)
   LINUX_KERNEL_VERSION = 3.8.13.29
endif

ifneq ($(NO_HARDWARE_CONCURRENCY),1)
   export NUM_BUILD_THREADS ?= $(shell cat /proc/cpuinfo | grep -c processor | awk '{print $$1}')
   export HARDWARE_CONCURRENCY := --jobs=$(NUM_BUILD_THREADS)
endif

PKG_CONFIG=$(SCRIPTS_DIR)/cross-pkg-config
export PKG_CONFIG

export TARGETFS_INSTALL_DIR
export EMBUILD_BASE
export BUILD_ROOT
export DOWNLOADS_DIR
export SCRIPTS_DIR
export ROOTFS_PACKAGE
export SUPPORT_XORG

export CROSS_COMPILE_DIR
export CROSS_COMPILE_TRIPLET
export CROSS_COMPILE_PREFIX
export CROSS_COMPILE
export ARM_FLOAT_ABI
export PLATFORM_FLAGS
export TARGET_ARCH
export TARGET_PLATFORM

export LINUXKERNEL_INSTALL_DIR
export LINUX_DEVKIT_PATH
export LINUX_KERNEL_VERSION

export SGX_SDK_DIR

export QT_DIR
export QT_HOSTPREFIX
export QT_TARGETFS_DIR
export QT_INSTALL_DIR
export QT_LIB
export QT_INC
export QT_VER

