all: check-build-prerequisites

ifeq ($(BUILD_ROOT),)
    $(error Variable BUILD_ROOT must not be empty)
endif

ifdef VERBOSE
   Q :=
else
   Q := @
endif

BUILD_PREREQUISITES_DIR := $(BUILD_ROOT)/prerequisites

DIRECTORIES_TO_CREATE += $(BUILD_PREREQUISITES_DIR)

.PHONY: check-build-prerequisites recheck-build-prerequisites clean-build-prerequisites help-build-prerequisites

help: help-build-prerequisites
help-build-prerequisites:
	@echo " "
	@echo "   check-build-prerequisites"
	@echo "                       - checks all build prerequisites;"
	@echo "   recheck-build-prerequisites"
	@echo "                       - forces re-check of all build prerequisites;"
	@echo "   clean-build-prerequisites"
	@echo "                       - cleans the status all build prerequisites;"

check-build-prerequisites: $(BUILD_ROOT) $(BUILD_PREREQUISITES_DIR)

recheck-build-prerequisites: clean-build-prerequisites check-build-prerequisites

clean-build-prerequisites:
	$(Q)rm -rf "$(BUILD_PREREQUISITES_DIR)"

HOST_MULTIARCH := $(shell dpkg --print-architecture)

MANDATORY_PACKAGES = 
DEPENDENCY_PACKAGES =

