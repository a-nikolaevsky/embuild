1. Create initramfs image. For example, boot target root filesystem through
nfs and call
    update-initramfs -c -k 2.6.37.
2. Add loading initramfs image to bootcmd. For example,
    setenv bootcmd 'mmc rescan 0; nand read 0x81000000 0x280000 0x300000; logo on 0x81000000 0xA0000000 0x81600000 40 60; fatload mmc 0:1 0x81000000 uImage; fatload mmc 0:1 0x82000000 initrd.img-2.6.37; bootm 0x81000000'
3. Add initramfs image to the kernel bootargs. For example,
    setenv bootargs 'initrd=0x82000000,<size-of-initrd.img-2.6.37-in-bytes> console=ttyO2,115200n8 rw ip=dhcp root=/dev/mapper/vg0-root rootfstype=ext4 ddr_mem=1024M mem=128M mem=960M@0xc0000000 vram=24M ti816xfb.vram=0:24M ti816xfb.debug=y vmalloc=776M notifyk.vpssm3_sva=0xBEE00000 vpss.blacklist=yes'
