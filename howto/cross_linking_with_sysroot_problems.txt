1. Some lib*.so are not real libraries but just a script for a GNU ld tool.
Such script may contain absolute paths redirecting a linker to building machine file system.
Error message looks like the following:
/usr/lib/gcc/arm-linux-gnueabihf/4.7/../../../../arm-linux-gnueabihf/bin/ld: cannot find /lib/arm-linux-gnueabihf/libpthread.so.0 inside 
/usr/lib/gcc/arm-linux-gnueabihf/4.7/../../../../arm-linux-gnueabihf/bin/ld: cannot find /usr/lib/arm-linux-gnueabihf/libpthread_nonshared.a inside 

Solution: open <rootfs>/usr/lib/arm-linux-gnueabihf/libpthread.so and replace
the absolute paths in the following like with libraries base names.
E.g. from
GROUP ( /lib/arm-linux-gnueabihf/libpthread.so.0 /usr/lib/arm-linux-gnueabihf/libpthread_nonshared.a )
to
GROUP ( libpthread.so.0 libpthread_nonshared.a )

See http://xc0ffee.wordpress.com/2011/04/28/linker-%E2%80%9Cld-cannot-find-liblibpthread-so-0%E2%80%B3/

2. Some libraries in the <rootfs> may be symbolic links with absolute paths.
They make linker pick up libraries from the build machine root causing
"undefined symbols", "incorrect format" or "no such file" errors depending on
whether build machine contain the referenced file or not and the target it was
built for.

Solution: replace absolute link with relative.

3. It may be useful to make linker more verbose adding -Wl,--verbose to the
compiler flags.
