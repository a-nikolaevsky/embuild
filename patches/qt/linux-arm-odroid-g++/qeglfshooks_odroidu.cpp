#include "qeglfshooks.h"
#include <EGL/fbdev_window.h>
 
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
 
#include <private/qcore_unix_p.h>
 
QT_BEGIN_NAMESPACE
 
class QEglFSOdroidHooks : public QEglFSHooks
{
private:
    void fbInit();
public:
    void platformInit() Q_DECL_OVERRIDE;
    EGLNativeWindowType createNativeWindow(QPlatformWindow *window, const QSize &size, const QSurfaceFormat &format) Q_DECL_OVERRIDE;
    void destroyNativeWindow(EGLNativeWindowType window) Q_DECL_OVERRIDE;
};
 
void QEglFSOdroidHooks::fbInit()
{
    int fd = qt_safe_open("/dev/fb0", O_RDWR, 0);
    if (fd == -1)
        qWarning("Failed to open fb to detect screen resolution!");
 
    struct fb_var_screeninfo vinfo;
    memset(&vinfo, 0, sizeof(vinfo));
    if (ioctl(fd, FBIOGET_VSCREENINFO, &vinfo) == -1)
        qWarning("Could not get variable screen info");
 
    vinfo.bits_per_pixel   = 32;
    vinfo.red.length       = 8;
    vinfo.green.length     = 8;
    vinfo.blue.length      = 8;
    vinfo.transp.length    = 8;
    vinfo.blue.offset      = 0;
    vinfo.green.offset     = 8;
    vinfo.red.offset       = 16;
    vinfo.transp.offset    = 24;
    vinfo.yres_virtual     = 2 * vinfo.yres;
 
    if (ioctl(fd, FBIOPUT_VSCREENINFO, &vinfo) == -1)
        qErrnoWarning(errno, "Unable to set double buffer mode!");
 
    qt_safe_close(fd);
    return;
}
 
void QEglFSOdroidHooks::platformInit()
{
    QEglFSHooks::platformInit();
    fbInit();
}
 
EGLNativeWindowType QEglFSOdroidHooks::createNativeWindow(QPlatformWindow *window, const QSize &size, const QSurfaceFormat &format)
{
    fbdev_window *fbwin = reinterpret_cast<fbdev_window *>(malloc(sizeof(fbdev_window)));
    if (NULL == fbwin)
        return 0;
 
    fbwin->width = size.width();
    fbwin->height = size.height();
    return (EGLNativeWindowType)fbwin;
}
 
void QEglFSOdroidHooks::destroyNativeWindow(EGLNativeWindowType window)
{
    free((void*)window);
}
 
QEglFSOdroidHooks eglFSOdroidHooks;
QEglFSHooks *platformHooks = &eglFSOdroidHooks;
 
QT_END_NAMESPACE

