.PHONY: all clean distclean help help-common git-clean kernel kernel_clean kermod kermod_clean

include Rules.make

all:
help: help-common
clean:
distclean:

DIRECTORIES_TO_CREATE := $(BUILD_ROOT)

include build_prerequisites_pre.mak

git-clean:
	$(Q)if [ -e "$(EMBUILD_BASE)/.env.mak" ]; \
	then \
	    trap "mv -f /tmp/.env.mak.$$$$ '$(EMBUILD_BASE)/.env.mak'" exit && mv "$(EMBUILD_BASE)/.env.mak" /tmp/.env.mak.$$$$; \
	fi && \
	git clean -dfx

help-common:
	@echo "This makefile creates a distribution of all neccessary software for $(BOARD_NAME) including:"
	@echo "   - Linux kernel $(LINUX_KERNEL_VERSION);"
	@echo "   - U-Boot binary image;"
	@echo "   - root filesystem for platform $(TARGET_PLATFORM) (architecture: $(TARGET_ARCH)) with pre-installed"
	@echo "      * Qt For Embedded $(QT_VER),"
	@echo "Available targets:"
	@echo "   all                 - incrementally builds all software;"
	@echo "   clean               - cleans all leaving root filesystem and build configuration files untouched;"
	@echo "   distclean           - complete clean, including root filesystem and build configuration files;"
	@echo "   git-clean           - removes all untracked and ignored files, including root filesystem and build configuration files;"
	@echo "   help                - this message;"

include rootfs.mak
include kernel.mak
include gfx_sdk.mak
include qt.mak

ifneq ($(NGP_HOME),)
    -include $(EMBUILD_BASE)/NGP.mak
endif

include build_prerequisites_post.mak

$(DIRECTORIES_TO_CREATE):
	@mkdir -p $@

distclean:
	$(Q)rm -rf $(BUILD_ROOT)

