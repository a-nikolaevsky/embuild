#!/bin/bash

usage ()
{
   echo "USAGE: $0 <gfx-sdk-dir> <gfx-sdk-ver> [<downloads-dir>]
The scripts looks for package with graphics SDK binaries of version <gfx-sdk-dir> in <downloads-dir>
and install them into directory <gfx-sdk-dir>.
"
}

if [[ "-h" = "$1" || "--help" = "$1" || -z "$1" || -z "$2" || -z "$3" ]]
then
   usage
   exit 1
fi

PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )":$PATH
. tar.inc

GFX_SDK_DIR=$1
GFX_SDK_VER=$2
DOWNLOADS_DIR=${3:-$PWD}
VERSION_SUFFIX=v$GFX_SDK_VER
PACKAGE_PREFIX="Mali_OpenGL_ES_SDK_"
PACKAGE_SUFFIX="Linux_x64.tar.gz"
URL="http://malideveloper.arm.com/develop-for-mali/sdks/opengl-es-sdk-for-linux/"

install_from_archive ()
{
   mkdir -p "$GFX_SDK_DIR" 
   [ -e "$1" ] && tar_extract "$1" -C "$GFX_SDK_DIR"
}

if [ ! -e "$DOWNLOADS_DIR/gfx_sdk/"${PACKAGE_PREFIX}${VERSION_SUFFIX}*_${PACKAGE_SUFFIX} ]
then
   mkdir -p "$DOWNLOADS_DIR/gfx_sdk"
   DL_URL="$(wget -c --no-check-certificate -O - "$URL" | perl -n -e 'print $1, "\n" if /\s+href="([^"]+'"${PACKAGE_SUFFIX}"')"/')"
   if [ -z "$DL_URL" ]
   then
       echo "ERROR: cannot find Graphics SDK download link"
       exit 1
   fi
   wget -P "$DOWNLOADS_DIR/gfx_sdk/" --no-check-certificate "$DL_URL" || exit $?
fi
if ! install_from_archive "$DOWNLOADS_DIR/gfx_sdk/"${PACKAGE_PREFIX}${VERSION_SUFFIX}*_${PACKAGE_SUFFIX}
then
  echo "ERROR: cannot find valid package with Graphics SDK ${GFX_SDK_VER} binaries in $DOWNLOADS_DIR/gfx_sdk directory" >&2
  exit 1
fi

