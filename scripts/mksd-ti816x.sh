#!/bin/bash

[[ -z "$EMBUILD_BASE" ]] && echo "ERROR: EMBUILD_BASE is not set. Please, make sure that $0 is called from EMBUILD_BASE environment" >&2 && exit 1
[[ root = $USER ]] || exec sudo -E $0 "$@"

PATH="$SCRIPTS_DIR:$PATH" . on-exit.inc

set_defaults () {
    MLO_PATH="$EMBUILD_BASE/tftphome/MLO"
    UBOOT_BIN_PATH="$EMBUILD_BASE/tftphome/u-boot_dvr.bin"
    KERNEL_UIMAGE_PATH="$EMBUILD_BASE/tftphome/uImage_DM816X_UD_DVR"
    ROOTFS_PACK_PATH="$ROOTFS_PACKAGE"
    UBOOT_SCRIPT_PATH="$EMBUILD_BASE/howto/u-boot_boot_from_sd.txt"
    SKIP_UBOOT_SCRIPT=false
    INITRAMFS_IMAGE_PATH=
    FILE=
    DRIVE=
}

validate_arguments () {
    for f in "$MLO_PATH" "$UBOOT_BIN_PATH" "$KERNEL_UIMAGE_PATH" "$ROOTFS_PACK_PATH" $($SKIP_UBOOT_SCRIPT || echo $UBOOT_SCRIPT_PATH) ${INITRAMFS_IMAGE_PATH:+"$INITRAMFS_IMAGE_PATH"}
    do
        if [[ ! -r "$f" ]]
        then
            echo "ERROR: file '$f' cannot be read" >&2
            return 1
        fi
    done
    local target="$1"
    [[ -z "$target" ]] && usage && return 1
    if [[ ! -e "$target" ]]
    then
        local targetDir=$(cd "${target%/*}" && pwd)
        if [[ $targetDir == /dev/* || $targetDir = /dev ]]
        then
            echo "ERROR: Device '$target' does not exists" >&2
            return 2
        fi
        FILE="$target"
    elif [[ ! -w "$target" ]]
    then
        echo "ERROR: cannot write to '$target'" >&2 && return 3
    elif [[ -c "$target" ]]
    then
        echo "ERROR: character devices are not supported" >&2 && return 4
    elif [[ -b "$target" ]]
    then
        DRIVE="$target"
    else
        FILE="$target"
    fi
}

release_loop_device()
{
   kpartx -d "$FILE" 
   losetup -d "$DRIVE"
}

on_exit () {
    for cmd in "$onExitCmds[@]"
    do
        eval $cmd
    done
}

usage () {
   set_defaults
   echo "USAGE: $0 [-m <path/to/MLO>] [-u <path/to/u-boot.bin>] [-s <path/to/u-boot/scripts>| -S] [-k <path/to/uImage>] [-r <path/to/rootfs/package>] [-i <path/to/initramfs/image>] path/to/block/device/or/file"
   echo "If target file is not a block device, then memory card image will be stored into specified file using loop device."
   echo "Options:"
   echo "   -m path to X-loader binary usually named MLO (default: $MLO_PATH)"
   echo "   -u path to U-Boot binary (default: $UBOOT_BIN_PATH)"
   echo "   -s path to U-Boot boot script (default: $UBOOT_SCRIPT_PATH)"
   echo "   -S do not use U-Boot boot script (default: $SKIP_UBOOT_SCRIPT)"
   echo "   -k path to Linux kernel uImage (default: $KERNEL_UIMAGE_PATH)"
   echo "   -r path to the package with a root filesystem (default: $ROOTFS_PACK_PATH)"
   echo "   -i path to the initramfs compressed image (default: $INITRAMFS_IMAGE_PATH)"
}

copy_boot_files () {
    echo "[Copying boot files...]"

    local MOUNT_POINT=$(mktemp -d)
    invokeOnExit umount $MOUNT_POINT
    removeOnExit $MOUNT_POINT

    mount $MOUNT_OPT "$PART_BOOT" $MOUNT_POINT &&
    cp "$MLO_PATH" $MOUNT_POINT/MLO &&
    cp "$UBOOT_BIN_PATH" $MOUNT_POINT/u-boot_dvr.bin &&
    cp "$KERNEL_UIMAGE_PATH" $MOUNT_POINT/uImage || return $?
    [[ -z "$INITRAMFS_IMAGE_PATH" ]] || cp "$INITRAMFS_IMAGE_PATH" $MOUNT_POINT/ || return $?
    $SKIP_UBOOT_SCRIPT && return 0
    
    echo "[Generating boot script...]"
    cp "$UBOOT_SCRIPT_PATH" $MOUNT_POINT/boot.txt &&
    mkimage -A arm -O linux -T script -C none -n TI_script -d "$UBOOT_SCRIPT_PATH" $MOUNT_POINT/boot.scr &&
    echo -e "Use the following U-Boot commands to boot:\n\tmmc rescan 0\n\tfatload mmc 0 0x81000000 boot.scr\n\tsource 0x81000000" || return $?
}

extract_rootfs () {
    echo "[Extracting root filesystem...]"
    local MOUNT_POINT=$(mktemp -d)
    invokeOnExit umount $MOUNT_POINT
    removeOnExit $MOUNT_POINT

    mount $MOUNT_OPT "$PART_ROOT" $MOUNT_POINT &&
    tar -xvf "$ROOTFS_PACK_PATH" -C $MOUNT_POINT || return $?
}

set_defaults

while getopts ":hm:u:k:r:Ss:i:" opt; do
  case $opt in
    h)
      usage
      exit 1
      ;;
    m)
      MLO_PATH="$OPTARG"
      ;;
    u)
      UBOOT_BIN_PATH="$OPTARG"
      ;;
    k)
      KERNEL_UIMAGE_PATH="$OPTARG"
      ;;
    r)
      ROOTFS_PACK_PATH="$OPTARG"
      ;;
    s)
      UBOOT_SCRIPT_PATH="$OPTARG"
      ;;
    S)
      SKIP_UBOOT_SCRIPT=true
      ;;
    i)
      INITRAMFS_IMAGE_PATH="$OPTARG"
      ;;
    \?)
      echo "E: Invalid option: -$OPTARG" >&2
      usage
      exit 1
      ;;
  esac
done

shift $((OPTIND-1))

validate_arguments "$1" || exit $?

if [[ -n "$FILE" ]]
then
   losetup --associated "$FILE" && exit 1
   dd if=/dev/zero of="$FILE" bs=64M count=32
   
   DRIVE=`(ls -1 /dev/loop[0-9]*; losetup -a | sed 's@:.*$@@') | sort | uniq -u | tail -1`
   losetup "$DRIVE" "$FILE"
   invokeOnExit release_loop_device
else
    egrep -w "^${DRIVE}[12]" /proc/mounts | awk '{print $1}' | while read D; do echo "[Unmounting $D ...]" && umount $D || exit $?; done || exit $?
fi


echo "[Partitioning $DRIVE...]"

dd if=/dev/zero of="$DRIVE" bs=1024 count=1024 || exit $?
	 
SIZE=`fdisk -l $DRIVE | grep Disk | awk '{print $5}'`
	 
echo DISK SIZE - $SIZE bytes
 
CYLINDERS=`echo $SIZE/255/63/512 | bc`
 
echo CYLINDERS - $CYLINDERS
{
echo ,9,0x0C,*
echo ,,,-
} | sfdisk -D -H 255 -S 63 -C $CYLINDERS $DRIVE || exit $?

if [[ -n "$FILE" ]]
then
    #MOUNT_OPT="-o loop"
    LOOP=( `kpartx -va "$FILE" | awk '{print $3}'` )
    PART_BOOT=/dev/mapper/${LOOP[0]}
    PART_ROOT=/dev/mapper/${LOOP[1]}
else
    PART_BOOT="${DRIVE}1"
    PART_ROOT="${DRIVE}2"
fi

echo "[Making filesystems...]"

mkfs.vfat -F 32 -n boot "$PART_BOOT" &&
mkfs.ext4 -L rootfs "$PART_ROOT" || exit $?

extract_rootfs && copy_boot_files || exit $?

echo "[Done]"
