#!/bin/bash

[[ -z "$1" ]] && echo "ERROR: guard.inc must be included with module name passed through the first argument" >&2 && exit 1
declare -f module_${1}_included >/dev/null && return 0
eval "module_${1}_included () { :; }"
