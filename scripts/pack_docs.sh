#!/bin/sh

usage () {
   echo "USAGE: $0 [<output-directory>]"
}

if [ -h = "$1" ] || [ --help = "$1" ]
then
   usage
   exit 1
fi

PACKAGE=${1:-$BUILD_ROOT}/$(basename "$EMBUILD_BASE")_docs.tar.bz2

mkdir -p "$(dirname "$PACKAGE")" 2>/dev/null

eval tar -cjf "$PACKAGE" -C "$EMBUILD_BASE" $(git ls-files -o --directory -i $(for P in `awk '/# *[dD]ocumentation/{docs=1;next}/^ *$/{docs=0}{if(docs){print}}' "$EMBUILD_BASE/.gitignore"`; do echo "-x $P"; done) | while read F; do echo "'"$F"'"; done) && echo "Documentation package $PACKAGE has been generated successfully"
     
