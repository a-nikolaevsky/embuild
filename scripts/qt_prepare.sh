#!/bin/bash

if [[ -z $1 || "-h" = $1 || "--help" = $1 ]]
then
   echo "USAGE: $0 <qt-dir>"
   exit 1
fi

QT_DIR="$1"

if [[ -z "$EMBUILD_BASE" ]]
then
   EMBUILD_BASE=`PATH=$(dirname $0):$PATH run-ti echo \\\${EMBUILD_BASE}`
   if [[ -z "$EMBUILD_BASE" ]]
   then
      echo "ERROR: Variable EMBUILD_BASE is not set" >&2
      exit 1
   fi
fi

. "$EMBUILD_BASE/scripts/qt-config-env" || exit 1

if [[ ! -d "$QT_DIR/.git" ]]
then
    echo "Cloning Qt repository into ${QT_DIR}..."
    git clone "${QT_MIRROR}" "${QT_DIR}" || exit 1
fi
cd "${QT_DIR}" &&
git checkout "$QT_BRANCH" &&
git clean -dxf &&
git reset --hard HEAD &&
git submodule foreach "git checkout" &&
git submodule foreach "git clean -dxf" &&
git submodule foreach "git reset --hard HEAD" &&
git fetch || exit 1
if [[ "$QT_HASH" ]]
then
    git reset --hard $QT_HASH
else
    git merge FETCH_HEAD
fi || exit 1
./init-repository -f &&
git submodule foreach "git fetch" &&
git submodule update --recursive || exit 1

for module in $NON_QT_SUBMODULES
do
    module_mirror="${module}_MIRROR"
    module_hash="${module}_HASH"
    module_branch="${module}_BRANCH"
    #if [[ -z "${!module_hash}" ]]
    #then
        #eval ${module}_HASH="$(cd "$QT_DIR" && git submodule status | awk '$2 == "'${module}'"{ sub("^-", "", $1); print $1; exit(0) }END{exit(1)}')" && eval ${module}_BRANCH=
    #fi
    if [ ! -d "$QT_DIR/$module/.git" ]
    then
        git clone "${!module_mirror}" "$QT_DIR/$module" || exit 1
    fi
    cd "$QT_DIR/$module" && git checkout ${!module_branch} && git clean -dxf && git reset --hard HEAD && git fetch || exit 1
    if [[ "${!module_hash}" ]]
    then
        git checkout ${!module_hash}
    else
        git merge FETCH_HEAD
    fi || exit 1
done

echo ==========================================================
cd "$QT_DIR" && git submodule status
echo ==========================================================
