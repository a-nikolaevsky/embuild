. guard.inc rootfs-hack.inc
. on-exit.inc
. tar.inc
. fakeroot.inc

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true LC_ALL=C LANGUAGE=C LANG=C 
declare -A CONFIG
TARGET_OS=
DISTRO=
ARCH=

put_conf ()
{
    CONFIG["$1"]="$2"
}

get_conf ()
{
    echo "${CONFIG[$1]}"
}

dump_conf ()
{
    local cfg
    echo "<platform-configuration>"
    for cfg in "${!CONFIG[@]}"
    do
        echo "    <$cfg>${CONFIG[$cfg]}</$cfg>"
    done
    echo "</platform-configuration>"
}

apply_platform_configuration ()
{
    local platform="$1"
    echo "I: parsing platform \`$platform' configuration"
    local config="${EMBUILD_BASE}/rootfs/${platform}/config"
    [[ ! -e "$config" ]] && echo "E: root filesystem configuration is not available for platform \`$platform'" >&2 && return 1
    . "$config" || return $?
    put_conf platform "$platform"
    TARGET_OS=$(get_conf os)
    DISTRO=$(get_conf suite)
    ARCH=$(get_conf arch)
    dump_conf
    [[ -n "$TARGET_OS" && -n "$TARGET_ARCH" && -n "$DISTRO" ]]
 }

set_rootfs_build_root ()
{
    ROOTFS_BUILD_ROOT="${BUILD_ROOT}/${TARGET_OS}-$DISTRO-$ARCH.$$"
    invokeOnExit keep_rootfs_build_root_on_error
    mkdir -p "${ROOTFS_BUILD_ROOT}"
}

keep_rootfs_build_root_on_error ()
{
    getExitCode
    if [[ 0 != $? && -n "$KEEP_ROOTFS_ON_ERROR" ]]
    then
        echo "W: root filesystem build directory $ROOTFS_BUILD_ROOT was not cleaned up due to KEEP_ROOTFS_ON_ERROR environment variable"
    else
        rm -rf "${ROOTFS_BUILD_ROOT}"
    fi
}

create_tempfile ()
{
    tempfile --directory "${ROOTFS_BUILD_ROOT}" ${1:+--prefix=$1} ${2:+--suffix=$2}
}

edit_rootfs_file ()
{
   local fname="$TARGETFS_DIR/$1"
   if [[ -e "$fname" ]]
   then
      echo "I: Patching file /$1"
   else
      echo "I: Creating file /$1"
      fake_sudo mkdir -p "$(dirname "$fname")" 2>/dev/null
      fake_sudo touch "$fname" || return $?
   fi
   local functor=$2
   local temp=$(create_tempfile ${1##*/} edit)
   removeOnExit $temp
   shift 2
   $functor "$@" < "$fname" > $temp || return $?
   chmod --reference="$fname" $temp
   fake_sudo chown --reference="$fname" $temp
   fake_sudo cp -fp $temp "$fname"
}

create_rootfs_package ()
{
   echo "I: Packing target root filesystem to $OUTPUT"
   mkdir -p "$(dirname "$OUTPUT")" 2>/dev/null
   rootfs_compress "$OUTPUT" .
}

list_config_directory_files ()
{
    find "${EMBUILD_BASE}/rootfs/${TARGET_OS}/$1/" -maxdepth 1 ! \( -type d -o -name "*~" -o -name "*.bak" -o -name ".*" \)
}

list_config_files ()
{
    local include_non_dev_only="${1:-false}"
    shift
    for dir in "$@"
    do
        list_config_directory_files "$dir" || return $?
        $include_non_dev_only || list_config_directory_files "$dir-dev" || return $?
    done
}

list_package_requirements ()
{
    local include_non_dev_only="$1"
    local include_versioned="${2:-false}"
    local opt="--include-non-versioned"
    ! $include_versioned || opt="--include-versioned"
    list_config_files "$include_non_dev_only" packages | xargs cat | "${EMBUILD_BASE}/scripts/list_package_requirements" ${opt}
}

foreach_external_package_repository ()
{
    local repo_id repo_mirror repo_keyring repo_components
    while read repo_id repo_mirror repo_keyring repo_components
    do
        [[ "$repo_id" == \#* || -z "$repo_id" ]] && continue;
        "$@" || return $?
    done << EOF
$(get_conf extra-repositories)
EOF
}

generate_multistrap_conf_section ()
{
    local line
    while read line; do
	eval echo $line >> "$MULTISTRAPCONF"
    done < "${EMBUILD_BASE}/rootfs/${TARGET_OS}/multistrap-extra.conf"
    extra_bootstrap+="${repo_id}_bootstrap "
    extra_aptsources+="${repo_id} "
}

install_packages ()
{
    local MULTISTRAPCONF="$(create_tempfile strap conf)"

    local extra_bootstrap extra_aptsources

    local PACKAGES="$@"
    local MIRROR=$(get_conf mirror)
    echo -n > "$MULTISTRAPCONF"
    foreach_external_package_repository generate_multistrap_conf_section
    local line
    while read line; do
	eval echo $line >> "$MULTISTRAPCONF"
    done < "${EMBUILD_BASE}/rootfs/${TARGET_OS}/multistrap.conf"

    # download and extract packages
    echo "I: run multistrap"
    local proxy=$(get_conf proxy)
    http_proxy=${proxy:+$proxy/${TARGET_OS}} fake_sudo multistrap -f "${MULTISTRAPCONF}" &&
    rootfs_run apt-get clean
}

process_non_versioned_package_requirements ()
{
    install_packages $(list_package_requirements)
}

update_package_system ()
{
   echo "I: Updating package system"
   rootfs_run apt-get -q update
}

check_additional_packages_consistency ()
{
   echo "I: Checking additional packages consistency ${@:+[$@] }"
   read -d '' script <<EOF
exit_code=0
for P in $(list_package_requirements)
do
   if ! dpkg-query --status \$P >/dev/null 2>&1 
   then
      echo "ERROR: Package \$P is missing" 2>&1
      exit_code=1
      continue
   fi
   dpkg -L \$P | while read F
      do
         if [ ! -e "\$F" ]
         then
            echo "ERROR: File \$F from package \$P is missing" >&2
            exit_code=1
         fi
   done
done
exit \$exit_code
EOF
   rootfs_run bash -c "$script"
}

fn_exists ()
{
   declare -f $1 > /dev/null
}

invoke ()
{
   "$@"
   local exit_code=$?
   if [[ 0 = $exit_code ]]
   then
      echo OK
   else
      echo FAILED
   fi
   return $exit_code
}

is_version_greater_or_equal ()
{
   dpkg --compare-versions "$1" ge "$2"
}

get_package_installed_version ()
{
    rootfs_run dpkg-query --show --showformat='${Status} ${Version}\n' $1 2>/dev/null | awk '/^install ok installed/{print $NF}'
}

get_package_available_version ()
{
    rootfs_run apt-cache show $1 2>/dev/null | awk '/^Version: /{print $2}'
}

install_package_version ()
{
    if ! is_version_greater_or_equal "$(get_package_installed_version $1)" $2
    then
        is_version_greater_or_equal "$(get_package_available_version $1)" $2 && packages_to_install[${#packages_to_install[@]}]=$1
    fi
}

add_fresher_distribution_to_package_repository ()
{
   # not implemented yet
   return 1
}

require_minimal_package_version ()
{
   local package_id_pattern=$1
   local version=$2
   local package_id
   printf -v package_id $package_id_pattern ""
   echo "I: Ensuring package $package_id version is at least $version"
   ! install_package_version $package_id $version || return 0
   printf -v package_id $package_id_pattern $version
   while ! install_package_version $package_id $version
   do
      add_fresher_distribution_to_package_repository || return $?
   done
}

process_versioned_package_requirements ()
{
    echo "I: Processing packages minimal version requirements"
    list_package_requirements false true | {
        local package_id_pattern version packages_to_install
        while read package_id_pattern version
        do
            [[ -z "$package_id_pattern$version" ]] || invoke require_minimal_package_version $package_id_pattern $version || return $?
        done
        [[ 0 = ${#packages_to_install[@]} ]] || install_packages "${packages_to_install[@]}"
    }
}

configure_packages ()
{
    # preseed debconf
    echo "I: preseed debconf"
    if [ -r "${EMBUILD_BASE}/rootfs/${TARGET_OS}/debconfseed.txt" ]; then
	cat "${EMBUILD_BASE}/rootfs/${TARGET_OS}/debconfseed.txt" | rootfs_run debconf-set-selections || return $?
    fi

    # run preinst scripts
    for script in $TARGETFS_DIR/var/lib/dpkg/info/*.preinst; do
	[ "$script" = "$TARGETFS_DIR/var/lib/dpkg/info/bash.preinst" ] && continue
	echo "I: run preinst script ${script##$TARGETFS_DIR}"
        rootfs_run env DPKG_MAINTSCRIPT_NAME=preinst DPKG_MAINTSCRIPT_PACKAGE=$(basename "${script%.preinst}") "${script##$TARGETFS_DIR}" install || return $?
    done

    # run dpkg --configure -a twice because of errors during the first run
    echo "I: configure packages"
    rootfs_run /usr/bin/dpkg --configure -a || rootfs_run /usr/bin/dpkg --configure -a
}

copy_initial_root_tree ()
{
    echo "I: copy initial directory root tree"
    tar_compress - --exclude-backups --exclude-vcs --exclude "*~" --exclude .gitempty --exclude .gitignore --owner root --group root -C "${EMBUILD_BASE}/rootfs/${TARGET_OS}/root" . | rootfs_extract -
}

prepare_fakeroot_environment ()
{
    echo "I: Preparing fakeroot environment"
    local src="${EMBUILD_BASE}/rootfs/${TARGET_OS}/fakeroot/"
    CLEANUP_FAKEROOT_ENV="${ROOTFS_BUILD_ROOT}/fakeroot-cleanup.sh"
    echo "rm '$TARGETFS_DIR/lib64'" > "$CLEANUP_FAKEROOT_ENV"
    find -L "$src" ! -type d | {
        while read file
        do
            local dest_file="${TARGETFS_DIR%/}/${file##$src}"
           if [[ -e "$dest_file" ]]
           then
               echo "mv '$dest_file.REAL' '$dest_file'" >> "$CLEANUP_FAKEROOT_ENV" &&
               fake_sudo mv "$dest_file" "$dest_file.REAL" || return $?
           else
               echo "rm '$dest_file'" >> "$CLEANUP_FAKEROOT_ENV" || return $?
           fi
           fake_sudo cp --dereference "$file" "$dest_file" || return $?
        done
    }
}

cleanup_fakeroot_environment ()
{
    echo "I: Cleaning up fakeroot environment"
    fake_sudo . "$CLEANUP_FAKEROOT_ENV"
}

count_path_items ()
{
    echo ${1%/} | { path_depth=1; while read -d / path_item; do ((path_depth++)); done; echo $path_depth; }
}

list_hooks()
{
    local path_depth=$(count_path_items "${EMBUILD_BASE}/rootfs/${TARGET_OS}/hooks/hook")
    list_config_files "$1" hooks | sort --field-separator=/ --key=$path_depth
}

run_hooks ()
{
    echo "I: running hooks"
    list_hooks "$1" | {
        while read hook
        do
            echo "I: invoking hook ${hook##*/}"
            invoke . "$hook" || return $?
        done
    }
}
