. guard.inc tar.inc

tar_generic () {
   ${GET_CMDLINE:+echo} tar ${TAR_CMD} --auto-compress --file "$@"
}

tar_extract () {
   TAR_CMD=--extract tar_generic "$@"
}

tar_compress () {
   TAR_CMD=--create tar_generic "$@"
}
