#!/bin/bash

TARGETFS_DIR="${TARGETFS_INSTALL_DIR}"
OUTPUT=
DO_REPACK=false
CREATE_ROOTFS=false
SETUP_PLATFORM=false

usage () {
   echo "USAGE: $0 [-s <sysroot>] [-c] [-T|-t <target-platform>] [-a <architecture>] [-P|-p <path/to/package>] <action1> [<action2> [...]]"
   echo "where <action-i> is command modifying rootfs (see $EMBUILD_BASE/scripts/rootfs-hack.inc for available commands)."
   echo "Options:"
   echo "   -s target filesystem directory (default: $TARGETFS_DIR)"
   echo "   -T setup environment for target platform configuration TARGET_PLATFORM=$TARGET_PLATFORM"
   echo "   -t setup environment for specified platform configuration"
   echo "   -P re-pack rootfs package rfs-<platform>.tar.gz after rootfs modification"
   echo "   -p re-pack specified rootfs package after rootfs modification"
   echo "   -c create rootfs if it does not exist"
}

while getopts ":hPp:ca:Tt:s:" opt; do
  case $opt in
    h)
      usage
      exit 1
      ;;
    c)
      CREATE_ROOTFS=true
      ;;
    a)
      export TARGET_ARCH=$OPTARG
      ;;
    T)
      SETUP_PLATFORM=true
      ;;
    t)
      SETUP_PLATFORM=true
      export TARGET_PLATFORM=$OPTARG
      ;;
    p)
      DO_REPACK=true
      OUTPUT="$OPTARG" && [[ -e "$OUTPUT" ]] && OUTPUT=`realpath -s "$OUTPUT"`
      ;;
    P)
      DO_REPACK=true
      ;;
    s)
      export TARGETFS_DIR=`realpath -s $OPTARG`
      ;;
    \?)
      echo "E: Invalid option: -$OPTARG" >&2
      usage
      exit 1
      ;;
   :)
      case "$OPTARG" in
         *)
            echo "E: Option -$OPTARG requires argument" >&2
            usage
            exit 1
            ;;
      esac
  esac
done

shift $((OPTIND-1))

if [[ -z "$1" ]] && ! $DO_REPACK
then
   usage
   exit 1
fi

[[ -n "$OUTPUT" ]] || OUTPUT="rfs-${TARGET_PLATFORM}.tar.gz"
[[ -n "$BUILD_ROOT" ]] || BUILD_ROOT=/tmp
PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )":$PATH
. rootfs-hack.inc
. fakeroot.inc

if [[ ! -e "$TARGETFS_DIR" ]]
then
   if ! $CREATE_ROOTFS
   then
      echo "E: target root filesystem does not exists at $TARGETFS_DIR" >&2
      exit 1
   fi
   if [[ ! -e "$OUTPUT" ]]
   then
      create_rootfs --arch "$TARGET_ARCH" --package "$OUTPUT" "$TARGET_PLATFORM" || exit $?
   fi
   mkdir -p "$TARGETFS_DIR" && rootfs_extract "$OUTPUT"
fi

rootfs_sync_to_bootable_snapshot ()
{
    local snapshot="$1"
    [[ -n "$snapshot" ]] || return $?
    echo "I: Generating root filesystem bootable snapshot \`$snapshot'"
    [[ ! -e "$snapshot" ]] || sudo rm -rf "$snapshot" || return $?
    mkdir -p "$snapshot" &&
    rootfs_compress - . | sudo tar --extract -C "$snapshot" --same-owner --same-permissions
}

rootfs_sync_from_bootable_snapshot ()
{
    local snapshot="$1"
    [[ -e "$snapshot" ]] || return $?
    echo "I: Synchronizing with root filesystem bootable snapshot \`$snapshot'"
    rootfs_remove &&
    mkdir -p "$TARGETFS_DIR" &&
    sudo tar --create -C "$snapshot" . | rootfs_extract -
}

[[ -z "$@" ]] || { 
    if $SETUP_PLATFORM
    then
         apply_platform_configuration "$TARGET_PLATFORM" && set_rootfs_build_root || return $?
    fi
    "$@"
} || exit $?

if $DO_REPACK
then
   create_rootfs_package || exit $?
fi
