#!/bin/sh

export PATH=$QT_DIR/qtbase/bin:$PATH
HOSTPREFIX="${QT_HOSTPREFIX:-${BUILD_ROOT}/qt}"
rm -rf ${QT_DIR}/qtbase/Makefile "$HOSTPREFIX" 2>/dev/null
mkdir -p "$HOSTPREFIX"
cd "$HOSTPREFIX"
XPARAMS=
if [ "1" = "$SUPPORT_XORG" ]
then
    XPARAMS=-xcb
else
    XPARAMS="-no-xcb -qpa eglfs"
fi
"$QT_DIR/configure" -prefix "$QT_TARGETFS_DIR" -hostprefix "$HOSTPREFIX" -force-pkg-config -release -make libs -xplatform devices/linux-arm-odroid-g++ -opengl es2 -confirm-license -opensource $XPARAMS -force-pkg-config -sysroot "$TARGETFS_INSTALL_DIR" -compile-examples -verbose -skip qtqa -skip qttools -skip qtdoc -developer-build
