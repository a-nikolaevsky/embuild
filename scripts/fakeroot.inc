. guard.inc fakeroot.inc
. tar.inc

get_fakeroot_context ()
{
    local rootfs="${1:-${TARGETFS_DIR:-${TARGETFS_INSTALL_DIR}}}"
    echo ${rootfs:+${rootfs%/}.fakeroot}
}

fake_sudo ()
{
    local rootfs="${TARGETFS_DIR:-${TARGETFS_INSTALL_DIR}}"
    [[ -n "$rootfs" ]] || return 1
    local fakeroot_context="$(get_fakeroot_context "$rootfs")"
    local load_context_opt
    [[ ! -e "$fakeroot_context" ]] || load_context_opt=(-i "${fakeroot_context}")
    PATH="/usr/sbin:/sbin:$PATH" QEMU_LD_PREFIX="$rootfs" fakeroot "${load_context_opt[@]}" -s "${fakeroot_context}" -- "$@"
}

get_non_opt_argument ()
{
    typeset -i arg_no=$1
    shift
    for arg in "$@"
    do
        [[ ! "$arg" == -* ]] && (( 0 == --arg_no )) && echo "$arg" && break
    done
}

fake_chroot ()
{
    local rootfs="$(get_non_opt_argument 1 "$@")"
    TARGETFS_DIR="${rootfs:-$TARGETFS_DIR}" fake_sudo fakechroot -- chroot "$@"  
}

fake_debootstrap ()
{
    local rootfs="$(get_non_opt_argument 2 "$@")"
    TARGETFS_DIR="${rootfs:-$TARGETFS_DIR}" fake_sudo fakechroot -- debootstrap ${rootfs:+--variant=fakechroot} "$@"  
}

rootfs_compress ()
{
    local dest="$1" archive="$1"
    shift
    local rootfs="${TARGETFS_DIR:-${TARGETFS_INSTALL_DIR}}"
    if [[ - != "$archive" ]]
    then
        dest="./tmp/rootfs.$$/${archive##*/}" &&
        mkdir -p "${rootfs}/${dest%/*}" &&
        removeOnExit "${rootfs}/${dest%/*}"
    fi
    # need to generate tar inside fakechroot so that absolute symlinks are correct
    fake_chroot "$rootfs" $(GET_CMDLINE=yes tar_compress "$dest" --numeric-owner --exclude "$dest" --exclude ./run/* -C / "$@") || return $?
    [[ - = "$archive" ]] || ln -f "${rootfs}/${dest}" "$archive"
}

rootfs_extract ()
{
    local archive="$1"
    shift
    local rootfs="${TARGETFS_DIR:-${TARGETFS_INSTALL_DIR}}"
    TARGETFS_DIR="$rootfs" fake_sudo $(GET_CMDLINE=yes tar_extract "$archive" -C "${rootfs}" --same-owner --same-permissions "$@" )
}

rootfs_remove ()
{
    local rootfs="${1:-${TARGETFS_DIR:-${TARGETFS_INSTALL_DIR}}}"
    [[ -n "$rootfs" ]] || return 1
    local fakeroot_context="$(get_fakeroot_context "$rootfs")"
    rm -rf "$rootfs" "$fakeroot_context" 2>/dev/null
}

rootfs_run ()
{
    fake_chroot "${TARGETFS_DIR:-${TARGETFS_INSTALL_DIR}}" "$@"
}
