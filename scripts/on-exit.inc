. guard.inc on-exit.inc
. fakeroot.inc

EXIT_CODE=-1

removeOnExit ()
{
    local f
    for f in "$@"
    do
        local prefix
        [[ / = ${f:0:1} ]] || prefix="$PWD"
        filesToRemove[${#filesToRemove[@]}]="${prefix}${f}"
    done
}

invokeOnExit ()
{
   onExitActions[${#onExitActions[@]}]="$*"
}

onExit ()
{
   EXIT_CODE=$?
   local action
   for action in "${onExitActions[@]}"
   do
      eval "$action"
   done

   if (( 0 < ${#filesToRemove[@]} ))
   then
      local do_fakeroot=$(get_fakeroot_context)
      ${do_fakeroot:+fake_sudo} rm -rf "${filesToRemove[@]}" 2>/dev/null
   fi
}

getExitCode()
{
    return $EXIT_CODE
}

trap onExit exit

