#!/bin/bash

setup-conf ()
{
mkdir "$TARGETFS_INSTALL_DIR/opt/bin" 2>/dev/null
cat << _EOF >"$TARGETFS_INSTALL_DIR/opt/bin/qt-env.sh"
#!/bin/sh

export QT_QPA_FONTDIR=$QT_TARGETFS_DIR/lib/fonts
#export LD_LIBRARY_PATH=$QT_TARGETFS_DIR/lib:\$LD_LIBRARY_PATH
export QT_DEBUG_PLUGINS=1
export QT_PLUGIN_PATH=$QT_TARGETFS_DIR/plugins/
_EOF
chmod +x "$TARGETFS_INSTALL_DIR/opt/bin/qt-env.sh"
}

update_ldconfig ()
{
   if [ /usr != $QT_TARGETFS_DIR ]
   then
      echo $QT_TARGETFS_DIR/lib >"$TARGETFS_INSTALL_DIR/etc/ld.so.conf.d/qt.conf" || return $?
   fi
}

setup-conf && update_ldconfig

