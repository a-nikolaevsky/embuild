.PHONY: kernel_build kernel kernel_config kernel_menuconfig kernel_uImage kernel_clean kernel_install kernel_firmware_install kernel_headers_install kernel_modules_install kernel_update help-kernel kernel_perf kernel_modules

$(LINUXKERNEL_INSTALL_DIR):
	$(Q)git clone --branch odroid-$(shell echo $(LINUX_KERNEL_VERSION) | cut -d. -f-2).y https://github.com/hardkernel/linux "$(LINUXKERNEL_INSTALL_DIR)"

kernel_update:
	$(Q)cd "$(LINUXKERNEL_INSTALL_DIR)" && git pull

kernel_build:
	$(Q)$(MAKE) -C "$(LINUXKERNEL_INSTALL_DIR)" CROSS_COMPILE=$(CROSS_COMPILE) ARCH=arm $(TARGET)

kernel_config:
	$(Q)$(MAKE) kernel_build TARGET=$(BOARD_TYPE)_defconfig

$(foreach target, headers_install menuconfig, kernel_$(target)):
	$(Q)$(MAKE) kernel_build TARGET=$(@:kernel_%=%)

$(foreach target, uImage modules clean distclean, kernel_$(target)):
	$(Q)$(MAKE) kernel_build $(HARDWARE_CONCURRENCY) TARGET=$(@:kernel_%=%)

$(foreach target, modules_install firmware_install, kernel_$(target)):
	$(Q)$(MAKE) kernel_build INSTALL_MOD_PATH="$(TARGETFS_INSTALL_DIR)" TARGET=$(@:kernel_%=%)

kernel_install: kernel_firmware_install kernel_headers_install kernel_modules_install

kernel: $(LINUXKERNEL_INSTALL_DIR) kernel_config kernel_uImage kernel_modules
	$(Q)install -D "$(LINUXKERNEL_INSTALL_DIR)/arch/arm/boot/uImage" "$(EMBUILD_BASE)/tftphome/uImage_$(BOARD_TYPE)"

kernel_perf:
	$(Q)$(MAKE) kernel_build TARGET=tools/perf

all: kernel
distclean: kernel_distclean
clean: kernel_clean

help: help-kernel
help-kernel:
	@echo " "
	@echo "   kernel              - builds $(BOARD_NAME) kernel $(LINUX_KERNEL_VERSION);"
	@echo "   kernel_config       - sets default $(BOARD_NAME) kernel config;"
	@echo "   kernel_menuconfig   - sets $(BOARD_NAME) kernel config interactively;"
	@echo "   kernel_clean        - cleans kernel;"
	@echo "   kernel_distclean    - cleans kernel;"
	@echo "   kernel_firmware_install"
	@echo "                       - installs kernel firmware into root filesystem;"
	@echo "   kernel_headers_install"
	@echo "                       - installs kernel headers into root filesystem;"
	@echo "   kernel_modules_install"
	@echo "                       - installs kernel modules into root filesystem;"
	@echo "   kernel_update       - updates kernel sources;"
	@echo "   kernel_perf         - builds perf profiling utility;"
