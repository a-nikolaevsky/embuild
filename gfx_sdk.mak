.PHONY: gfx_sdk_binaries gfx_sdk_all gfx_sdk_clean gfx_sdk_build gfx_sdk_install _notify_gfx_sdk_install gfx_sdk_upgrade gfx_sdk_kermod gfx_sdk_kermod_clean gfx_sdk-check-build-prerequisites

GFX_SDK_BINARIES_DIRS := $(GFX_SDK_DIR)/lib/arm
GFX_SDK_SRC_DIR := $(GFX_SDK_DIR)/lib/arm/
GFX_SDK_LIB_DIR := $(GFX_SDK_SRC_DIR)/build
GFX_SDK_INC_DIR := $(GFX_SDK_DIR)/inc

DIRECTORIES_TO_CREATE += $(GFX_SDK_LIB_DIR)

ifneq ($(SUPPORT_XORG),1)
    GFX_HEADERS_TO_INSTALL := $(shell find "$(GFX_SDK_INC_DIR)" -name '*.h' | sed "s@$(GFX_SDK_INC_DIR)@$(TARGETFS_INSTALL_DIR)/usr/include@")
    GFX_LIBS_TO_INSTALL := $(foreach lib, EGL GLESv1_CM GLESv2, $(TARGETFS_INSTALL_DIR)/usr/lib/$(CROSS_COMPILE_TRIPLET)/lib$(lib).so)
    GFX_PKGCONFIG_TO_INSTALL := $(foreach pc, egl glesv1_cm glesv2, $(TARGETFS_INSTALL_DIR)/usr/lib/$(CROSS_COMPILE_TRIPLET)/pkgconfig/$(pc).pc)

$(GFX_HEADERS_TO_INSTALL) : $(TARGETFS_INSTALL_DIR)/usr/include/% : $(GFX_SDK_INC_DIR)/%
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh fake_sudo install -D "$<" "$@"

$(GFX_LIBS_TO_INSTALL) : $(TARGETFS_INSTALL_DIR)/usr/lib/$(CROSS_COMPILE_TRIPLET)/% : $(GFX_SDK_LIB_DIR)/%
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh fake_sudo install "$<" "$@"

$(GFX_PKGCONFIG_TO_INSTALL) : $(TARGETFS_INSTALL_DIR)/usr/lib/$(CROSS_COMPILE_TRIPLET)/% : $(EMBUILD_BASE)/patches/gfx_sdk/%
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh fake_sudo install "$<" "$@"
endif

$(TARGETFS_INSTALL_DIR)/usr/include/EGL/fbdev_window.h: $(GFX_SDK_DIR)/simple_framework/inc/mali/EGL/fbdev_window.h
	$(Q)$(SCRIPTS_DIR)/update_rootfs.sh fake_sudo install -D "$<" "$@"

GFX_FILES_TO_INSTALL = $(GFX_HEADERS_TO_INSTALL) $(GFX_LIBS_TO_INSTALL) $(GFX_PKGCONFIG_TO_INSTALL) $(TARGETFS_INSTALL_DIR)/usr/include/EGL/fbdev_window.h 

MANDATORY_PACKAGES += cmake

gfx_sdk_binaries: $(GFX_SDK_BINARIES_DIRS)

$(GFX_SDK_BINARIES_DIRS):
	@echo "Cleaning up previous $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK installation binaries (if any)..."
	$(Q)rm -rf $(GFX_SDK_BINARIES_DIRS)
	@echo Installing $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK $(GFX_SDK_VER) binaries...
	$(Q)$(GFX_SDK_BIN_INSTALL_CMD)
	@echo Patching $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK $(GFX_SDK_VER)...
	for patch_file in $(EMBUILD_BASE)/patches/gfx_sdk/post-setup/*.patch; do \
	   patch <$$patch_file; \
	done

gfx_sdk_all: gfx_sdk_binaries gfx_sdk_build gfx_sdk_install

gfx_sdk_build: $(GFX_SDK_LIB_DIR)
	@echo Making $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK $(GFX_SDK_VER)...
	$(Q)cd "$(GFX_SDK_LIB_DIR)" && cmake "$(GFX_SDK_SRC_DIR)" -DCMAKE_TOOLCHAIN_FILE="$(GFX_SDK_DIR)/arm-linux.cmake" -DTOOLCHAIN_ROOT="$(CROSS_COMPILE_DIR)/bin/$(CROSS_COMPILE_TRIPLET)-" 
	$(Q)$(MAKE) -C $(GFX_SDK_LIB_DIR) $(HARDWARE_CONCURRENCY) all

_notify_gfx_sdk_install:
	@echo Installing $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK $(GFX_SDK_VER)...

gfx_sdk_install: _notify_gfx_sdk_install $(GFX_FILES_TO_INSTALL)

gfx_sdk_clean:
	@echo Cleaning $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK $(GFX_SDK_VER)...
	$(Q)$(MAKE) -C $(GFX_SDK_LIB_DIR) clean

gfx_sdk_distclean: gfx_sdk_clean
	$(Q)rm -rf $(GFX_SDK_BINARIES_DIRS)

gfx_sdk_kermod:
	@echo Making $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK $(GFX_SDK_VER) kernel modules...
	$(Q)echo "not implemented yet"

gfx_sdk_kermod_clean:
	@echo Cleaning $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK $(GFX_SDK_VER) kernel modules...
	$(Q)echo "not implemented yet"

gfx_sdk_upgrade: gfx_sdk_distclean gfx_sdk_all

kermod: gfx_sdk_kermod
kermod_clean: gfx_sdk_kermod_clean

help: help-gfx_sdk
help-gfx_sdk:
	@echo " "
	@echo "   gfx_sdk_binaries    - installs $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK $(GFX_SDK_VER) binaries into $(GFX_SDK_DIR) directory;"
	@echo "   gfx_sdk_all         - builds $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK libraries, kernel modules and demos and installs them into target root filesystem;"
	@echo "   gfx_sdk_build       - builds $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK libraries, kernel modules and demos;"
	@echo "   gfx_sdk_all         - installs $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK libraries, kernel modules and demos into target root filesystem;"
	@echo "   gfx_sdk_clean       - cleans $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK compilation directories;"
	@echo "   gfx_sdk_distclean   - cleans $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK compilation and binary installation directories;"
	@echo "   gfx_sdk_upgrade     - rebuilds $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK after version upgrade;"
	@echo "   gfx_sdk_kermod      - incrementally builds $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK kernel modules;"
	@echo "   gfx_sdk_kermod_clean- cleans $(GFX_SDK_VENDOR) $(GFX_SDK_DEVICE) SDK kernel modules;"

all: gfx_sdk_all
clean: gfx_sdk_clean
distclean: gfx_sdk_distclean

